from django.contrib.auth.forms import UserCreationForm as UCF

from users.models import User


class UserCreationForm(UCF):
    class Meta:
        model = User
        fields = ("email", )
