from datetime import datetime

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from users.models import User


def validate_date(value):
    if value.replace(tzinfo=None) < datetime.now().replace(tzinfo=None):
        raise ValidationError(_('date must be less than now.'))


class Event(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    date = models.DateTimeField(validators=[validate_date])
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('date', )

    def __str__(self):
        return u'{}'.format(self.title)

    @property
    def owner_username(self):
        return self.owner.email.split('@')[0]


class Rsvp(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('event', 'user')

    def __str__(self):
        return u'{}-{}'.format(self.event.title, self.user.username)
