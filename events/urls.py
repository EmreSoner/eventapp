from django.contrib.auth.decorators import login_required
from django.urls import path

from events.views import (EventCreate, EventList, EventDetail, EventUpdate,
                          EventDelete, create_rsvp, delete_rsvp)

urlpatterns = [
    path('create/', login_required(EventCreate.as_view()), name='event-create'),
    path('<int:pk>/update/', login_required(EventUpdate.as_view()), name='event-update'),
    path('<int:pk>/delete/', login_required(EventDelete.as_view()), name='event-delete'),
    path('<int:pk>', login_required(EventDetail.as_view()), name='event-detail'),

    path('<int:event_pk>/rsvp/create', login_required(create_rsvp), name='rsvp-create'),
    path('<int:event_pk>/rsvp/delete', login_required(delete_rsvp), name='rsvp-delete'),
]
