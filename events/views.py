from django.db.models import Count
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView
from django.utils.translation import gettext_lazy as _

from events.models import Event, Rsvp


class EventCreate(CreateView):
    model = Event
    fields = ('title', 'description', 'date')
    success_url = '/events/{id}'

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.owner = self.request.user
        instance.save()
        self.object = instance
        return HttpResponseRedirect(self.get_success_url())


class EventUpdate(UpdateView):
    model = Event
    fields = ('title', 'description', 'date')
    success_url = '/events/{id}'

    def get_object(self, *args, **kwargs):
        instance = super().get_object(*args, **kwargs)
        if instance.owner != self.request.user:
            raise PermissionError(_('You can not update this event.'))
        return instance


class EventDelete(DeleteView):
    model = Event
    success_url = reverse_lazy('event-list')

    def get_object(self, *args, **kwargs):
        instance = super().get_object(*args, **kwargs)
        if instance.owner != self.request.user:
            raise PermissionError(_('You can not delete this event.'))
        return instance


class EventList(ListView):
    model = Event
    paginate_by = 5

    def get_queryset(self):
        return Event.objects.select_related('owner').all().annotate(Count('rsvp'))


class EventDetail(DetailView):
    model = Event

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['already_rsvp'] = self.request.user.id in \
            self.get_object().rsvp_set.all().values_list('user_id', flat=True)
        return context


def create_rsvp(request, event_pk):
    event = get_object_or_404(Event, pk=event_pk)
    Rsvp.objects.get_or_create(event=event, user=request.user)
    return HttpResponseRedirect(reverse_lazy('event-detail', kwargs={'pk': event_pk}))


def delete_rsvp(request, event_pk):
    event = get_object_or_404(Event, pk=event_pk)
    rsvp = get_object_or_404(Rsvp, event=event, user=request.user)
    rsvp.delete()
    return HttpResponseRedirect(reverse_lazy('event-detail', kwargs={'pk': event_pk}))
