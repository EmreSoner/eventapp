from django.urls import reverse_lazy
from django.views.generic import CreateView

from users.forms import UserCreationForm


class SignUp(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'
